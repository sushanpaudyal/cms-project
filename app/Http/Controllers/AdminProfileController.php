<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Image;

class AdminProfileController extends Controller
{
    // Admin profile
    public function adminProfile(){
        $admin = Auth::guard('admin')->user();
        return view ('admin.profile', compact('admin'));
    }

    // Admin profile update
    public function adminProfileUpdate(Request $request, $id){
        $admin = Admin::findOrFail($id);
        $data = $request->all();
        $rules = [
            'admin_name' => 'required',
        ];
        $customMessages = [
          'admin_name.required' => 'Admin name Cannot be empty'
        ];
        $this->validate($request, $rules, $customMessages);
        $admin->admin_name = $data['admin_name'];
        $admin->phone = $data['phone'];
        $admin->address = $data['address'];

        $random = Str::random(10);
        if($request->hasFile('image')){
            $image_tmp = $request->file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $random.'.'.$extension;
                $image_path = 'public/uploads/'.$filename;
                Image::make($image_tmp)->save($image_path);
                $admin->image = $filename;
            }
        }
        $admin->save();
        Session::flash('info_message', 'Admin profile has Been updated successfully');
        return redirect()->back();

    }

    // Admin Update Password
    public function changePassword(){
        $user = Admin::where('email', Auth::guard('admin')->user()->email)->first();
        return view ('admin.changePassword', compact('user'));
    }

    // Update Password
    public function updatePassword(Request $request, $id){
        $data = $request->all();
        $validateData = $request->validate([
             'current_password' => 'required|max:255',
             'password' => 'min:6',
            'pass_confirmation' => 'required_with:password|same:password|min:6'
        ]);
        $user = Admin::where('email', Auth::guard('admin')->user()->email)->first();
        $current_user_password = $user->password;
        if(Hash::check($data['current_password'], $current_user_password)){
            $user->password = bcrypt($data['password']);
            $user->save();
            Session::flash('info_message', 'Your Password Has Been Updated');
            return redirect()->back();
        } else {
            Session::flash('error_message', 'Your Current Password Does Not match in our database');
            return redirect()->back();
        }

    }
}
