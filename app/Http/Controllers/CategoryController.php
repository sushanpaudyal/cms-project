<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use DataTables;

class CategoryController extends Controller
{
    // Index Page
    public function index(){
        return view ('admin.category.index');
    }

    // Add Category
    public function add(){
        return view ('admin.category.add');
    }

    // Store Category
    public function store(Request $request){
        $data = $request->all();
        $validateData = $request->validate([
            'category_name' => 'required|max:255',
        ]);
        $category = new Category();
        $category->category_name = $data['category_name'];
        $category->slug = Str::slug($data['category_name']);
        if(!empty($data['status'])){
            $category->status = 1;
        } else {
            $category->status = 0;
        }
        $category->save();
        Session::flash('info_message', 'New Category has been added');
        return redirect()->back();
    }

    // Datatable
    public function dataTable(){
        $model = Category::all();
        return DataTables::of($model)
            ->addColumn('action', function ($model){
                return view ('admin.category._actions', [
                     'model' => $model,
                    'url_edit' => route('category.edit', $model->id),
                    'url_delete' => route('category.delete', $model->id),
                ]);
            })
            ->editColumn('status', function ($model){
                if($model->status == 1){
                       return "Active";
                } else {
                    return "In Active";

                }
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    // Edit
    public function edit($id){
        $category = Category::findOrFail($id);
        return view ('admin.category.edit', compact('category'));
    }

    // Update Category
    public function update(Request $request, $id){
        $data = $request->all();
        $validateData = $request->validate([
            'category_name' => 'required|max:255',
        ]);
        $category = Category::findOrFail($id);
        $category->category_name = $data['category_name'];
        $category->slug = Str::slug($data['category_name']);
        if(!empty($data['status'])){
            $category->status = 1;
        } else {
            $category->status = 0;
        }
        $category->save();
        Session::flash('info_message', 'New Category has been updated');
        return redirect()->back();
    }

    // Delete
    public function delete($id){
        $category = Category::findOrFail($id);
        $category->delete();
        Session::flash('error_message', 'Category has been deleted');
        return redirect()->back();
    }
}
