<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use DataTables;

class TagController extends Controller
{
    // Index Page
    public function index(){
        return view ('admin.tag.index');
    }
    // Add New Tag
    public function add(){
        return view ('admin.tag.add');
    }

    // Store Tag
    public function store(Request  $request){
        $data = $request->all();
        $validateData = $request->validate([
            'tag_name' => 'required|max:255',
        ]);
        $tag = new Tag();
        $tag->tag_name = $data['tag_name'];
        $tag->slug = Str::slug($data['tag_name']);
        if(!empty($data['status'])){
            $tag->status = 1;
        } else {
            $tag->status = 0;
        }
        $tag->save();
        Session::flash('info_message', 'New Tag has been added');
        return redirect()->back();
    }

    // Datatable
    public function dataTable(){
        $model = Tag::all();
        return DataTables::of($model)
            ->addColumn('action', function ($model){
                return view ('admin.tag._actions', [
                    'model' => $model,
                    'url_edit' => route('tag.edit', $model->id),
                    'url_delete' => route('tag.delete', $model->id),
                ]);
            })
            ->editColumn('status', function ($model){
                if($model->status == 1){
                    return "Active";
                } else {
                    return "In Active";

                }
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }

    // Edit
    public function edit($id){
        $tag = Tag::findOrFail($id);
        return view ('admin.tag.edit', compact('tag'));
    }

    // Update Category
    public function update(Request $request, $id){
        $data = $request->all();
        $validateData = $request->validate([
            'tag_name' => 'required|max:255',
        ]);
        $tag = Tag::findOrFail($id);
        $tag->tag_name = $data['tag_name'];
        $tag->slug = Str::slug($data['tag_name']);
        if(!empty($data['status'])){
            $tag->status = 1;
        } else {
            $tag->status = 0;
        }
        $tag->save();
        Session::flash('info_message', 'Tag has been updated');
        return redirect()->back();
    }

    // Delete
    public function delete($id){
        $tag = Tag::findOrFail($id);
        $tag->delete();
        Session::flash('error_message', 'Tag has been deleted');
        return redirect()->back();
    }
}
