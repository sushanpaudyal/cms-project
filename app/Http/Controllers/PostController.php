<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Image;
use DataTables;

class PostController extends Controller
{
    // Index Page
    public function index(){
        return view ('admin.post.index');
    }

    // Add Post
    public function add(){
        $categories = Category::where('status', 1)->get();
        $tags = Tag::where('status', 1)->get();
        return view ('admin.post.add', compact('categories', 'tags'));
    }

    // Store Post
    public function store(Request $request){
        $data = $request->all();
        $post = new Post();
        $post->category_id = $data['category_id'];
        $post->post_title = $data['post_title'];
        $post->slug = Str::slug($data['post_title']);
        $post->post_content = $data['post_content'];

        $post->seo_title = $data['seo_title'];
        $post->seo_subtitle = $data['seo_subtitle'];
        $post->seo_keywords = $data['seo_keywords'];
        $post->seo_description = $data['seo_description'];
        $post->admin_id = Auth::guard('admin')->user()->id;
        if(!empty($data['status'])){
            $post->status = 1;
        } else {
            $post->status = 0;
        }


        $slug = Str::slug($data['post_title']);
        $random = rand(1,999999);
        if($request->hasFile('image')){
            $image_tmp = $request->file('image');
            if($image_tmp->isValid()){
                $extension = $image_tmp->getClientOriginalExtension();
                $filename = $slug .'-'. $random . '.' . $extension;
                $image_path = 'public/uploads/posts/'.$filename;
                Image::make($image_tmp)->save($image_path);
                $post->image = $filename;
            }
        }

        $tags = $data['tag_id'];
         $post->save();
        $post->tags()->attach($tags);
        Session::flash('info_message', 'New Post has been added');
        return redirect()->back();

    }


    // Datatable
    public function dataTable(){
        $model = Post::all();
        return DataTables::of($model)
            ->addColumn('action', function ($model){
                return view ('admin.post._actions', [
                    'model' => $model,
                    'url_edit' => route('post.edit', $model->id),
                    'url_delete' => route('post.delete', $model->id),
                ]);
            })
            ->editColumn('category_id', function ($model){
              return $model->category->category_name;
            })
            ->addIndexColumn()
            ->rawColumns(['action'])
            ->make(true);
    }
}
