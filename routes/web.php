<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::prefix('/admin')->group(function(){
    // Admin Login Page
    Route::match(['get', 'post'],'/login', 'AdminLoginController@login')->name('adminLogin');

    Route::group(['middleware' => 'admin'], function (){
    // Admin Dashboard
    Route::get('/dashboard', 'AdminLoginController@dashboard')->name('adminDashboard');
    // Admin Profile
    Route::get('/profile', 'AdminProfileController@adminProfile')->name('adminProfile');
    // Admin Profile Update
    Route::post('/profile/update/{id}', 'AdminProfileController@adminProfileUpdate')->name('adminProfileUpdate');
    // Admin Password Update
    Route::get('/profile/change_password', 'AdminProfileController@changePassword')->name('changePassword');
    // Admin Update Password
    Route::post('/profile/update_password/{id}', 'AdminProfileController@updatePassword')->name('updatePassword');



    // Category Route
        Route::get('/category', 'CategoryController@index')->name('category.index');
        Route::get('/category/add', 'CategoryController@add')->name('category.add');
        Route::post('/category/store', 'CategoryController@store')->name('category.store');
        Route::get('/table/category', 'CategoryController@dataTable')->name('table.category');
        Route::get('/category/edit/{id}', 'CategoryController@edit')->name('category.edit');
        Route::post('/category/update/{id}', 'CategoryController@update')->name('category.update');
        Route::get('/delete-category/{id}', 'CategoryController@delete')->name('category.delete');

    // Tag Route
        Route::get('/tag', 'TagController@index')->name('tag.index');
        Route::get('/tag/add', 'TagController@add')->name('tag.add');
        Route::post('/tag/store', 'TagController@store')->name('tag.store');
        Route::get('/table/tag', 'TagController@dataTable')->name('table.tag');
        Route::get('/tag/edit/{id}', 'TagController@edit')->name('tag.edit');
        Route::post('/tag/update/{id}', 'TagController@update')->name('tag.update');
        Route::get('/delete-tag/{id}', 'TagController@delete')->name('tag.delete');

    // Post Route
        Route::get('/post', 'PostController@index')->name('post.index');
        Route::get('/post/add', 'PostController@add')->name('post.add');
        Route::post('/post/store', 'PostController@store')->name('post.store');
        Route::get('/table/post', 'PostController@dataTable')->name('table.post');
        Route::get('/post/edit/{id}', 'PostController@edit')->name('post.edit');
        Route::post('/post/update/{id}', 'PostController@update')->name('post.update');
        Route::get('/delete-post/{id}', 'PostController@delete')->name('post.delete');


        Route::post('ckeditor', 'CkeditorFileUploadController@store')->name('ckeditor.upload');
    });

    // Admin Logout
    Route::get('/logout', 'AdminLoginController@adminLogout')->name('adminLogout');
});
