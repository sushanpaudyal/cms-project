<a href="{{ $url_edit }}" class="btn btn-info" title="Edit : {{ $model->category_name }}">
  Edit
</a>

<a href="{{ $url_delete }}" class="btn btn-danger btn-delete" title="Delete : {{ $model->category_name }}" rel="{{ $model->id }}" rel1="delete-category">
    Delete
</a>
