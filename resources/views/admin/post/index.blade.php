@extends('admin.includes.admin_design')

@section('site_title') View All Post @endsection


@section('content')
    <!-- start page title -->
    <div class="row align-items-center">
        <div class="col-sm-6">
            <div class="page-title-box">
                <h4 class="font-size-18">Post</h4>
                <ol class="breadcrumb mb-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Post</a></li>
                    <li class="breadcrumb-item active">All Post</li>
                </ol>
            </div>
        </div>


        <div class="col-sm-6">
            <div class="float-right d-none d-md-block">
                <div class="dropdown">
                    <a href="{{ route('post.add') }}" class="btn btn-primary dropdown-toggle waves-effect waves-light" >
                        <i class="mdi mdi-plus mr-2"></i> Add New Post
                    </a>

                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->

    @include('admin.includes._message')


    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">


                    <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>S.N.</th>
                            <th>Post Title</th>
                            <th>Category</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                    </table>

                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->


@endsection

@section('js')
    <!-- Required datatable js -->
    <script src="{{ asset('public/backend/assets/libs/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('public/backend/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Datatable init js -->
    <script src="{{ asset('public/backend/assets/js/pages/datatables.init.js') }}"></script>



    <script>
        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            sorting: true,
            searchable :true,
            responsive: true,
            ajax: "{{ route('table.post') }}",
            columns: [
                {data: 'DT_RowIndex', name:'DT_RowIndex'},
                {data: 'post_title', name:'post_title'},
                {data: 'category_id', name:'category_id'},
                {data: 'action', name:'action', orderable: false},

            ]
        });

        $('body').on('click', '.btn-delete', function (event){
            event.preventDefault();
            var SITEURL = '{{ URL::to('') }}';
            var id = $(this).attr('rel');
            var deleteFunction = $(this).attr('rel1');
            swal({
                    title: "Are You Sure? ",
                    text: "You will not be able to recover this record again",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, Delete it!"
                },
                function () {
                    window.location.href =  SITEURL + "/admin/" + deleteFunction + "/" + id;
                });
        });
    </script>


@endsection
