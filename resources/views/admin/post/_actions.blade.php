<a href="{{ $url_edit }}" class="btn btn-info" title="Edit : {{ $model->post_title }}">
  Edit
</a>

<a href="{{ $url_delete }}" class="btn btn-danger btn-delete" title="Delete : {{ $model->post_title }}" rel="{{ $model->id }}" rel1="delete-post">
    Delete
</a>
